//
//  ArchiveAddSessionTableViewController.h
//  IPhotobooth
//
//  Created by Sastra Winarta on 14/10/12.
//
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>

@interface ArchiveAddSessionTableViewController : UITableViewController<RKObjectLoaderDelegate>
{
    NSArray *_imageNames;
}

@property (nonatomic, strong) NSMutableOrderedSet *selectionImages;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
