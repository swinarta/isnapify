//
//  IPhotoboothAppDelegate.h
//  IPhotobooth
//
//  Created by Sastra Winarta on 5/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IPhotoboothAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
