//
//  ArchiveAddSessionTableViewController.m
//  IPhotobooth
//
//  Created by Sastra Winarta on 14/10/12.
//
//

#import "ArchiveAddSessionTableViewController.h"
#import "Session.h"
#import "ApplicationSettings.h"

@interface ArchiveAddSessionTableViewController ()
@end

@implementation ArchiveAddSessionTableViewController

@synthesize tableView = _tableView;
@synthesize selectionImages = _selectionImages;

-(NSMutableOrderedSet *)selectionImages
{
    if(!_selectionImages)
    {
        _selectionImages = [[NSMutableOrderedSet alloc] init];
    }
    
    return _selectionImages;
}

- (void)loadArchives
{
//    [SVProgressHUD showWithStatus:@"Loading Archives" maskType:SVProgressHUDMaskTypeGradient];
    [[RKObjectManager sharedManager] loadObjectsAtResourcePath:@"/archives" delegate:self];
}

- (void)objectLoader:(RKObjectLoader *)objectLoader didFailWithError:(NSError *)error
{
//    NSLog(@"didFailWithError: %@", error);
//    [SVProgressHUD dismiss];
}

- (void)objectLoader:(RKObjectLoader*)objectLoader didLoadObjects:(NSArray*)objects
{
    _imageNames = objects;
    [self.tableView reloadData];
//    [SVProgressHUD dismiss];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_imageNames count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Archive Image Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    Session *session = (Session *)_imageNames[indexPath.row];

    ApplicationSettings *settings = [ApplicationSettings sharedInstance];
    
    NSString *imageURL = [[settings.serverURL stringByAppendingString:@"/"] stringByAppendingString:session.preview];
    
    cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]]];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSNumber *index = [NSNumber numberWithInt:indexPath.row];
    
    if([self.selectionImages indexOfObject:index] == NSNotFound){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [self.selectionImages addObject:index];
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
        [self.selectionImages removeObject:index];
    }
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadArchives];
    
    NSLog(@"load");
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload {
    NSLog(@"unload");
    [self setTableView:nil];
    [super viewDidUnload];
}
@end
