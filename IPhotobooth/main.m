//
//  main.m
//  IPhotobooth
//
//  Created by Sastra Winarta on 5/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IPhotoboothAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IPhotoboothAppDelegate class]));
    }
}
