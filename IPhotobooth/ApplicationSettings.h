//
//  ApplicationSettings.h
//  IPhotobooth
//
//  Created by Sastra Winarta on 30/9/12.
//
//

#import <Foundation/Foundation.h>

@interface ApplicationSettings : NSObject
@property (nonatomic, strong) NSString *serverURL;
@property (nonatomic, strong) NSArray *templateImages;
@property (nonatomic, strong) NSArray *templateRects;

+ (id)sharedInstance;
@end

