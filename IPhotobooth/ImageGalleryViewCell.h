//
//  ImageGalleryViewCell.h
//  IPhotobooth
//
//  Created by sastra winarta on 17/5/12.
//

#import <Foundation/Foundation.h>
#import "AQGridViewCell.h"

@interface ImageGalleryViewCell : AQGridViewCell
{
    UIImageView * _imageView;
}
@property (nonatomic, strong) UIImage * image;
@property (nonatomic, strong) id object;

@end
