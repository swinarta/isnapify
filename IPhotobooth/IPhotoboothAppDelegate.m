//
//  IPhotoboothAppDelegate.m
//  IPhotobooth
//
//  Created by Sastra Winarta on 5/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <RestKit/RestKit.h>
#import "IPhotoboothAppDelegate.h"
#import "ApplicationSettings.h"
#import "Session.h"
#import "Thumb.h"
#import "Photo.h"

@implementation IPhotoboothAppDelegate

@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    ApplicationSettings *settings = [ApplicationSettings sharedInstance];
    
    NSLog(@"%@", settings.serverURL);
    
    // Override point for customization after application launch.
    RKObjectManager* manager = [RKObjectManager managerWithBaseURLString:settings.serverURL];
    
    RKObjectMapping* sessionMapping = [RKObjectMapping mappingForClass:[Session class]];
    [sessionMapping mapKeyPathsToAttributes:
        @"name", @"name",
        @"count", @"count",
        @"preview", @"preview", nil];
    
    RKObjectMapping* thumbMapping = [RKObjectMapping mappingForClass:[Thumb class]];
    [thumbMapping mapKeyPathsToAttributes:
     @"name", @"name", nil];

    RKObjectMapping* photoMapping = [RKObjectMapping mappingForClass:[Photo class]];
    [photoMapping mapKeyPathsToAttributes:
     @"imageData", @"imageData",
     @"photoId", @"photoId",
     @"isArchive", @"isArchive",
     @"numberOfCopy", @"numberOfCopy" ,nil];

    [[RKObjectManager sharedManager].mappingProvider setObjectMapping:sessionMapping forResourcePathPattern:@"/archives"];

    [manager.mappingProvider registerMapping:sessionMapping withRootKeyPath:@""];
    
    [manager.mappingProvider setSerializationMapping:photoMapping forClass:[Photo class]];

    RKObjectRouter *router = manager.router;
    [router routeClass:[Session class] toResourcePathPattern:@"/sessions"];
    [router routeClass:[Thumb class] toResourcePathPattern:@"/thumbs/:id"];
    [router routeClass:[Photo class] toResourcePathPattern:@"/photos"];
    
    manager.router = router;

    [RKObjectManager setSharedManager:manager];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

@end
