//
//  TemplateImageView.h
//  IPhotobooth
//
//  Created by Sastra Winarta on 4/10/12.
//
//

#import <UIKit/UIKit.h>

@interface TemplateImageView : UIView

@property (nonatomic, strong) NSNumber *imageWidth;
@property (nonatomic, strong) NSNumber *imageHeight;
@property (nonatomic, strong) UIImage *templateImage;
@property (nonatomic, strong) NSArray *templateRects;
@property (nonatomic, strong) NSArray *templateRectsResized;

-(void) drawImage:(UIImage *) image;
-(void) drawSelectedImage:(UIImage *)selectedImage andTemplateIndex:(NSInteger) index;
-(UIImage *) imageToPrint;
-(void) animate;
@end
