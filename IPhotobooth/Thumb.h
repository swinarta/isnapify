//
//  Thumb.h
//  IPhotobooth
//
//  Created by Sastra Winarta on 10/10/12.
//
//

#import <Foundation/Foundation.h>

@interface Thumb : NSObject{
    NSString* _name;    
}

@property (nonatomic, strong) NSString* name;

@end
