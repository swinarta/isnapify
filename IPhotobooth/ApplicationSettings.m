//
//  ApplicationSettings.m
//  IPhotobooth
//
//  Created by Sastra Winarta on 30/9/12.
//
//

#import "ApplicationSettings.h"

@implementation ApplicationSettings

@synthesize serverURL = _serverURL;
@synthesize templateImages = _templateImages;
@synthesize templateRects = _templateRects;

-(NSString *) serverURL
{
    if(!_serverURL){
        _serverURL = @"http://192.168.17.163:8888";
        //_serverURL = @"http://172.20.10.6:8888";
        //_serverURL = @"http://localhost:8888";
        
        [self loadTemplates];
        
    }
    return _serverURL;
}

-(void) loadTemplates
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"template" ofType:@"plist"];
    NSMutableDictionary *templateDictionary = [[NSMutableDictionary alloc]initWithContentsOfFile:path];
    NSArray *sortedKeys = [[templateDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSMutableArray *templateImagesMutableArray = [[NSMutableArray alloc] initWithCapacity:sortedKeys.count];
    
    NSMutableArray *templateRectsMutableArray = [[NSMutableArray alloc] initWithCapacity:sortedKeys.count];
    
    [sortedKeys enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [templateImagesMutableArray addObject:[UIImage imageNamed:obj]];
        
        id rectValue = [templateDictionary objectForKey:obj];
        
        NSMutableArray *rectPlistValueArray = (NSMutableArray *)rectValue;
        NSMutableArray *rectArray = [[NSMutableArray alloc] initWithCapacity: rectPlistValueArray.count];
        
        [rectPlistValueArray enumerateObjectsUsingBlock:^(id obj_, NSUInteger idx_, BOOL *stop_){
            
            NSMutableArray *rectPlistArray = (NSMutableArray *)obj_;
            
            CGRect rect = CGRectMake([rectPlistArray[0] floatValue], [rectPlistArray[1] floatValue], [rectPlistArray[2] floatValue], [rectPlistArray[3] floatValue]);
            
            [rectArray addObject:[NSValue valueWithCGRect:rect]];
            
        }];
        
        [templateRectsMutableArray addObject:rectArray];
        
    }];
    
    _templateImages = [templateImagesMutableArray copy];
    _templateRects = [templateRectsMutableArray copy];    

}

static ApplicationSettings *sharedInstance = nil;

+ (ApplicationSettings *) sharedInstance
{

    if(!sharedInstance)
    {
        sharedInstance = [[super alloc]init];
    }
    
    return sharedInstance;
    
}
@end
