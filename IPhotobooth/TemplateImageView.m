//
//  TemplateImageView.m
//  IPhotobooth
//
//  Created by Sastra Winarta on 4/10/12.
//
//

#import <QuartzCore/QuartzCore.h>
#import "TemplateImageView.h"

@interface TemplateImageView ()
@property (nonatomic, strong) CALayer *templateLayer;
@property (nonatomic, strong) NSArray *rectLayers;
@property (nonatomic, strong) NSMutableArray *rectLayerImages;

@end

@implementation TemplateImageView

@synthesize imageWidth = _imageWidth;
@synthesize imageHeight = _imageHeight;
@synthesize templateImage = _templateImage;

@synthesize templateLayer = _templateLayer;
@synthesize templateRects = _templateRects;
@synthesize templateRectsResized = _templateRectsResized;

@synthesize rectLayers = _rectLayers;
@synthesize rectLayerImages = _rectLayerImages;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code        
    }
    return self;
}

- (CALayer *) templateLayer
{
    if(!_templateLayer)
    {
        _templateLayer = [CALayer layer];
        _templateLayer.frame = self.bounds;
    }
    
    return _templateLayer;
}

- (void) setTemplateRectsResized:(NSArray *)templateRects
{
    [_rectLayers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx_, BOOL *stop_) {
        [obj removeFromSuperlayer];
    }];

    NSMutableArray *content = [[NSMutableArray alloc] initWithCapacity: templateRects.count];
    
    [templateRects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx_, BOOL *stop_) {
        CALayer *layer = [CALayer layer];
        layer.frame = [obj CGRectValue];
        layer.backgroundColor = [[UIColor greenColor] CGColor];
        [self.layer addSublayer:layer];
        [content addObject: layer];
    }];
    
    [self.layer addSublayer:_templateLayer];
    
    _rectLayers = [content copy];
    
    _rectLayerImages = [[NSMutableArray alloc] initWithCapacity:templateRects.count];
    for (int i=0; i<templateRects.count; i++) {
        [_rectLayerImages addObject:[NSNull null]];
    }
    
    _templateRects = templateRects;
}

-(void) drawImage:(UIImage *) inputImage
{
    self.templateLayer.contents = (id)inputImage.CGImage;
    _templateImage = inputImage;
}

-(void) drawSelectedImage:(UIImage *)selectedImage andTemplateIndex:(NSInteger) index
{
    CALayer * layer = ((CALayer *)self.rectLayers[index]);
    layer.contents = (id)selectedImage.CGImage;
    _rectLayerImages[index] = selectedImage;
}

- (UIImage *) imageToPrint
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(_imageWidth.floatValue, _imageHeight.floatValue), NO, 1.0f);
        
    [self.templateRects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIImage *image = _rectLayerImages[idx];
        if(image && image != (id)[NSNull null])
        {
            [image drawInRect: [obj CGRectValue]];
        }
    }];    

    [_templateImage drawInRect:CGRectMake(0, 0, _imageWidth.floatValue, _imageHeight.floatValue) ];

    UIImage* resultImage = UIGraphicsGetImageFromCurrentImageContext();
    [resultImage drawInRect:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];

    UIGraphicsEndImageContext();
    return resultImage;
}

-(void) animate
{
    self.alpha = 0.8;
    [UIView beginAnimations:@"Fade-in" context:NULL];
    [UIView setAnimationDuration:0.4];
    self.alpha = 1.0;
    [UIView commitAnimations];
}
@end
