//
//  ArchiveViewController.m
//  IPhotobooth
//
//  Created by Sastra Winarta on 11/10/12.
//
//

#import "ArchiveViewController.h"
#import "SVProgressHUD.h"
#import "ImageGalleryViewCell.h"
#import "Session.h"
#import "ApplicationSettings.h"
#import "TemplateViewController.h"

@implementation ArchiveViewController

@synthesize archiveView = _archiveView;

#pragma mark RKObjectLoaderDelegate methods

- (void)loadArchives
{
    [SVProgressHUD showWithStatus:@"Loading Archives" maskType:SVProgressHUDMaskTypeGradient];
    [[RKObjectManager sharedManager] loadObjectsAtResourcePath:@"/archives" delegate:self];
}

- (void)objectLoader:(RKObjectLoader *)objectLoader didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    [SVProgressHUD dismiss];
}

- (void)objectLoader:(RKObjectLoader*)objectLoader didLoadObjects:(NSArray*)objects
{
    _imageNames = objects;
    [self.archiveView reloadData];
    [SVProgressHUD dismiss];
}


- (NSUInteger) numberOfItemsInGridView: (AQGridView *) aGridView
{
    return ( _imageNames.count );
}

- (AQGridViewCell *) gridView: (AQGridView *) aGridView
           cellForItemAtIndex: (NSUInteger) index
{
    
    Session *session = (Session *)_imageNames[index];
    
    static NSString * SessionCellIdentifier = @"SessionCellIdentifier";
    
    ImageGalleryViewCell * cell = (ImageGalleryViewCell *)[aGridView dequeueReusableCellWithIdentifier:SessionCellIdentifier];
    if ( cell == nil )
    {
        cell = [[ImageGalleryViewCell alloc] initWithFrame: CGRectMake(0.0, 0.0, 320.0, 226.0)                                                 reuseIdentifier: SessionCellIdentifier];
    }
    
    ApplicationSettings *settings = [ApplicationSettings sharedInstance];
    
    NSString *imageURL = [[settings.serverURL stringByAppendingString:@"/"] stringByAppendingString:session.preview];
    
    cell.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]]];
    cell.object = session;
    
    return cell;
}

- (void) gridView:(AQGridView *)gridView didSelectItemAtIndex:(NSUInteger)index
{
    ImageGalleryViewCell *cell = (ImageGalleryViewCell *)[gridView cellForItemAtIndex:index];
    [self performSegueWithIdentifier:@"ShowTemplate" sender: cell];
    [gridView deselectItemAtIndex:index animated:NO];
}

- (CGSize) portraitGridCellSizeForGridView: (AQGridView *) aGridView
{
    return ( CGSizeMake(320.0, 246.0) );
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ApplicationSettings *settings = [ApplicationSettings sharedInstance];
    
    ImageGalleryViewCell *cell = (ImageGalleryViewCell *)sender;
    TemplateViewController *templateController = (TemplateViewController *)segue.destinationViewController;
    templateController.sessionId = ((Session *)cell.object).name;
    templateController.templateImages = settings.templateImages;
    templateController.templateRects = settings.templateRects;
    templateController.isArchive = YES;
    templateController.hidesBottomBarWhenPushed = YES;
    self.navigationController.toolbarHidden = NO;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.archiveView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	self.archiveView.autoresizesSubviews = YES;
	self.archiveView.delegate = self;
	self.archiveView.dataSource = self;
    
}

- (void)viewDidUnload {
    [self setArchiveView:nil];
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.toolbarHidden = YES;

    [self loadArchives];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (IBAction)refreshPressed:(id)sender
{
    [self loadArchives];
}

@end
