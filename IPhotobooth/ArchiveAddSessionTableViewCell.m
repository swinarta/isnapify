//
//  ArchiveAddSessionTableViewCell.m
//  IPhotobooth
//
//  Created by Sastra Winarta on 15/10/12.
//
//

#import "ArchiveAddSessionTableViewCell.h"

@implementation ArchiveAddSessionTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(0, 0, 129, 91);
}

@end
