//
//  IPhotoboothViewController.h
//  IPhotobooth
//
//  Created by Sastra Winarta on 5/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <RestKit/RestKit.h>
#import <UIKit/UIKit.h>
#import "AQGridView.h"
#import "TemplateViewController.h"

@interface IPhotoboothViewController : UIViewController <AQGridViewDelegate, AQGridViewDataSource, RKObjectLoaderDelegate>
{
    NSArray *_imageNames;
}

@property (nonatomic, strong) IBOutlet AQGridView * galleryView;

@end
