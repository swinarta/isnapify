//
//  TemplateViewController.m
//  IPhotobooth
//
//  Created by Sastra Winarta on 2/10/12.
//
//

#import <dispatch/dispatch.h>
#import <RestKit/RestKit.h>
#import "TemplateViewController.h"
#import "NSDataAdditions.h"
#import "SVProgressHUD.h"
#import "ApplicationSettings.h"
#import "Thumb.h"
#import "Photo.h"

@interface TemplateViewController()
@property (nonatomic, strong) NSNumber *currentTemplateIndex;
@property (nonatomic, strong) NSArray *templateRectsResized;
@property (nonatomic, strong) NSNumber *currentSessionIndex;
@property (nonatomic, strong) UITextField *textField;
@end

@implementation TemplateViewController

@synthesize sessionId = _sessionId;
@synthesize templateImages = _templateImages;
@synthesize templateRects = _templateRects;
@synthesize templateRectsResized = _templateRectsResized;

@synthesize imageView = _imageView;
@synthesize printButton = _printButton;
@synthesize isArchive = _isArchive;

@synthesize templateView = _templateView;
@synthesize currentTemplateIndex = _currentTemplateIndex;
@synthesize sessionPhotos = _sessionPhotos;
@synthesize textField = _textField;

#define IMAGE_ORIGINAL_SIZE_WIDTH (1200)
#define IMAGE_ORIGINAL_SIZE_HEIGHT (800)

#pragma mark RKObjectLoaderDelegate methods

- (void)loadPhotos
{
    [SVProgressHUD showWithStatus:@"Downloading Photos" maskType:SVProgressHUDMaskTypeGradient];
    
    NSString *urlString = [NSString stringWithFormat:@"/thumbs/%@", self.sessionId];
    
    if(_isArchive){
        urlString = [urlString stringByAppendingString:@"?a=1"];
    }
    
    [[RKObjectManager sharedManager] loadObjectsAtResourcePath:urlString delegate:self];
}

- (void)objectLoader:(RKObjectLoader *)objectLoader didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    [SVProgressHUD dismiss];
}

- (void)objectLoader:(RKObjectLoader*)objectLoader didLoadObjects:(NSArray*)objects
{

    if(![objectLoader.sourceObject isKindOfClass:[Photo class]]){

        ApplicationSettings *settings = [ApplicationSettings sharedInstance];

        NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:3];
        
        [objects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            NSString *filename = ((Thumb *) obj).name;
            NSString *urlString;
            
            if(!_isArchive){
                urlString = [settings.serverURL stringByAppendingFormat:@"/data/sessions/%@/full/%@%@", self.sessionId, [filename substringToIndex: filename.length-4], @".jpg"];
            }else{
                urlString = [settings.serverURL stringByAppendingFormat:@"/data/archive/%@/full/%@%@", self.sessionId, [filename substringToIndex: filename.length-4], @".jpg"];
            }
            
            [result addObject:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString: urlString]]]];
            
        }];
        
        _sessionPhotos = [result copy];

    }else{

        //submitting photo to printer
        NSLog(@"Sending photo to printer");
        [self.navigationController popViewControllerAnimated:YES];
        
        
    }
    
    [SVProgressHUD dismiss];
}

-(NSNumber *) currentTemplateIndex
{
    if(!_currentTemplateIndex)
    {
        _currentTemplateIndex = [NSNumber numberWithInt:0];
    }
    
    return _currentTemplateIndex;
}

-(NSString *)sessionId
{
    if(!_sessionId)
    {
        //_sessionId = @"1335199148251";
    }
    
    return _sessionId;
}

-(void) setSessionId:(NSString *)sessionId
{
    _sessionId = sessionId;
}

-(void) setImageView:(TemplateImageView *)imageView
{
    _imageView = imageView;
    
    UISwipeGestureRecognizer *rightDoubleSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleRightTemplateSwipe:)];
    rightDoubleSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    rightDoubleSwipeGestureRecognizer.numberOfTouchesRequired = 2;

    UISwipeGestureRecognizer *leftDoubleSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftTemplateSwipe:)];
    leftDoubleSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    leftDoubleSwipeGestureRecognizer.numberOfTouchesRequired = 2;

    
    UISwipeGestureRecognizer *rightSingleSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleRightImageSwipe:)];
    rightSingleSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;

    UISwipeGestureRecognizer *leftSingleSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftImageSwipe:)];
    leftSingleSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;

    
    [self.imageView addGestureRecognizer:rightDoubleSwipeGestureRecognizer];
    [self.imageView addGestureRecognizer:leftDoubleSwipeGestureRecognizer];
    [self.imageView addGestureRecognizer:rightSingleSwipeGestureRecognizer];
    [self.imageView addGestureRecognizer:leftSingleSwipeGestureRecognizer];
    [self.imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTemplateTap:)]];

}

-(void) handleRightTemplateSwipe:(UISwipeGestureRecognizer *) gesture
{
    if(gesture.state == UIGestureRecognizerStateChanged ||
       gesture.state == UIGestureRecognizerStateEnded)
    {
        if(_currentTemplateIndex.intValue >= _templateImages.count-1)
        {
            _currentTemplateIndex = [NSNumber numberWithInt:0];
        }else{
            _currentTemplateIndex = [NSNumber numberWithInt:_currentTemplateIndex.intValue+1];
        }
                
        [self drawTemplateWithCurrentIndex];
                
    }
}

-(void) handleLeftTemplateSwipe:(UISwipeGestureRecognizer *) gesture
{
    if(gesture.state == UIGestureRecognizerStateChanged ||
       gesture.state == UIGestureRecognizerStateEnded)
    {
        if(_currentTemplateIndex.intValue <= 0)
        {
            _currentTemplateIndex = [NSNumber numberWithInt:_templateImages.count-1];
        }else{
            _currentTemplateIndex = [NSNumber numberWithInt:_currentTemplateIndex.intValue-1];
        }
        
        [self drawTemplateWithCurrentIndex];
    }
}

-(void) handleLeftImageSwipe:(UISwipeGestureRecognizer *) gesture
{
    
    if(gesture.state == UIGestureRecognizerStateChanged ||
       gesture.state == UIGestureRecognizerStateEnded)
    {
        CGPoint point = [gesture locationInView:self.imageView];
        NSInteger idxFound = [self point:point AtRectIndex: _templateRectsResized[_currentTemplateIndex.intValue]];

//        NSLog(@"idxFound:%i", idxFound);

        if(idxFound > -1)
        {
            if(!_currentSessionIndex){
                _currentSessionIndex = [NSNumber numberWithInt:0];
            }else{
                if(_currentSessionIndex.intValue <= 0)
                {
                    _currentSessionIndex = [NSNumber numberWithInt:self.sessionPhotos.count-1];
                }else{
                    _currentSessionIndex = [NSNumber numberWithInt:_currentSessionIndex.intValue-1];
                }
            }
                        
            [self drawTemplateWithCurrentIndexAndImageSelection:self.sessionPhotos[_currentSessionIndex.intValue] AndIndex:idxFound];

        }
        
//        NSLog(@"gesture point x:%g y:%g", point.x, point.y);
    }
}

-(void) handleRightImageSwipe:(UISwipeGestureRecognizer *) gesture
{
    
    if(gesture.state == UIGestureRecognizerStateChanged ||
       gesture.state == UIGestureRecognizerStateEnded)
    {
        CGPoint point = [gesture locationInView:self.imageView];
        NSInteger idxFound = [self point:point AtRectIndex: _templateRectsResized[_currentTemplateIndex.intValue]];
        
//        NSLog(@"idxFound:%i", idxFound);
        
        if(idxFound > -1)
        {
            if(!_currentSessionIndex){
                _currentSessionIndex = [NSNumber numberWithInt:0];
            }else{
                if(_currentSessionIndex.intValue >= self.sessionPhotos.count-1)
                {
                    _currentSessionIndex = [NSNumber numberWithInt:0];
                }else{
                    _currentSessionIndex = [NSNumber numberWithInt:_currentSessionIndex.intValue+1];
                }
            }
            
            [self drawTemplateWithCurrentIndexAndImageSelection:self.sessionPhotos[_currentSessionIndex.intValue] AndIndex:idxFound];
            
        }
        
//        NSLog(@"gesture point x:%g y:%g", point.x, point.y);
    }
}


-(void) handleTemplateTap:(UITapGestureRecognizer *)gesture
{
    /*
    NSLog(@"curridx:%i", _currentTemplateIndex.intValue);

    CGPoint point = [gesture locationInView:self.imageView];
    NSUInteger idxFound = [self point:point AtRectIndex: _templateRectsResized[_currentTemplateIndex.intValue]];
    NSLog(@"point x:%f, y:%f", point.x, point.y);
    NSLog(@"idxFound:%i", idxFound);
     */
}

-(NSUInteger) point:(CGPoint)point AtRectIndex:(NSArray *)rects
{
    __block NSUInteger retval = -1;
    [rects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        CGRect currRect = [obj CGRectValue];
        if(point.x >= currRect.origin.x && point.x <= currRect.origin.x + currRect.size.width
           && point.y >= currRect.origin.y && point.y <= currRect.origin.y + currRect.size.height){
            *stop = YES;
            retval = idx;
        }
    }];
    
    return retval;
}

-(void) drawTemplateWithCurrentIndexAndImageSelection:(UIImage *) selectedImage AndIndex:(NSInteger)index
{
    [_imageView drawSelectedImage:selectedImage andTemplateIndex:index];
}

-(void) drawTemplateWithCurrentIndex
{
    [_imageView drawImage:self.templateImages[self.currentTemplateIndex.intValue]];
    _imageView.templateRectsResized = _templateRectsResized[self.currentTemplateIndex.intValue];
    _imageView.templateRects = _templateRects[self.currentTemplateIndex.intValue];
    [_imageView animate];
}

-(void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
 
    int widthImg = IMAGE_ORIGINAL_SIZE_WIDTH;
    int heightImg = IMAGE_ORIGINAL_SIZE_HEIGHT;
 
    float widthRatio = _imageView.bounds.size.width/widthImg;
    float heightRatio = _imageView.bounds.size.height/heightImg;
    float ratioUsed;
 
    float widthActual;
    float heightActual;
 
    if(widthRatio >= heightRatio){
        ratioUsed = widthRatio;
        if(_imageView.bounds.size.width >= widthImg){
            widthActual = widthImg;
            heightActual = widthRatio * _imageView.bounds.size.height;
        }else{
            widthActual = _imageView.bounds.size.width;
            heightActual = widthRatio * heightImg;
        }
    }else{
        ratioUsed = heightRatio;
        if(_imageView.bounds.size.width >= widthImg){
            widthActual = heightRatio * widthImg;
            heightActual = _imageView.bounds.size.height;
        }else{
            widthActual = heightRatio * _imageView.bounds.size.width;
            heightActual = heightImg;
        }
    }
    
    _imageView.bounds = CGRectMake(0, 0, widthActual, heightActual);
    _imageView.imageWidth = [NSNumber numberWithFloat:IMAGE_ORIGINAL_SIZE_WIDTH];
    _imageView.imageHeight = [NSNumber numberWithFloat:IMAGE_ORIGINAL_SIZE_HEIGHT];
    
    

    NSMutableArray *mutableTemplateRectResized = [[NSMutableArray alloc] initWithCapacity:self.templateRects.count];
    
    [self.templateRects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        NSMutableArray *content = [[NSMutableArray alloc] initWithCapacity: ((NSArray *)obj).count];
        
        [obj enumerateObjectsUsingBlock:^(id obj_, NSUInteger idx_, BOOL *stop_) {
            
            CGRect oldRect = [obj_ CGRectValue];
            
            CGRect newRect = CGRectMake(oldRect.origin.x * ratioUsed, oldRect.origin.y * ratioUsed, oldRect.size.width * ratioUsed, oldRect.size.height * ratioUsed);
                        
            [content addObject: [NSValue valueWithCGRect:newRect]];
                        
        }];
        
        [mutableTemplateRectResized addObject:[content copy]];
        
    }];
    
    _templateRectsResized = [mutableTemplateRectResized copy];
    
    [self drawTemplateWithCurrentIndex];

}

- (IBAction)PrintPressed:(id)sender {

    [SVProgressHUD showWithStatus:@"Sending Photo to Printer" maskType:SVProgressHUDMaskTypeGradient];

    [self.printButton setEnabled:NO];
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Print Photo" message:@"Number Of Copy\n\n\n" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];

    self.textField = [[UITextField alloc] init];
    [self.textField setBackgroundColor:[UIColor whiteColor]];
    self.textField.delegate = self;
    self.textField.borderStyle = UITextBorderStyleLine;
    self.textField.frame = CGRectMake(15, 75, 255, 30);
    self.textField.font = [UIFont fontWithName:@"ArialMT" size:20];
    self.textField.textAlignment = UITextAlignmentCenter;
    self.textField.keyboardAppearance = UIKeyboardAppearanceAlert;
    self.textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    self.textField.text = @"1";
    
    [self.textField becomeFirstResponder];
    [alert addSubview:self.textField];
    
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString* detailString = self.textField.text;
    NSLog(@"String is: %@", detailString);
    NSNumber* mycopy = [NSNumber numberWithInt:[detailString intValue]];
    NSLog(@"Number is: %@", mycopy);

     dispatch_queue_t backgroundQueue = dispatch_queue_create("com.sastraw.queue", NULL);
     
     dispatch_async(backgroundQueue, ^(void) {
     
     Photo *photo = [[Photo alloc] init];
     photo.imageData = [UIImageJPEGRepresentation([_imageView imageToPrint], 1.0) base64Encoding];
     photo.photoId = _sessionId;
     photo.isArchive = _isArchive;
         photo.numberOfCopy = mycopy;
     
     [[RKObjectManager sharedManager] postObject:photo delegate:self];
     
     });
     
     dispatch_release(backgroundQueue);

    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadPhotos];
}

- (void)viewDidUnload {
    [self setTemplateView:nil];
    [self setImageView:nil];
    [self setPrintButton:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
