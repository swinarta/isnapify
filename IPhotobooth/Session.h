//
//  Sessions.h
//  IPhotobooth
//
//  Created by Sastra Winarta on 5/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Session : NSObject {  
    NSNumber* _count;  
    NSString* _name;  
    NSString* _preview;  
}  

@property (nonatomic, strong) NSNumber* count;  
@property (nonatomic, strong) NSString* name;  
@property (nonatomic, strong) NSString* preview;  

@end  