//
//  Photo.h
//  IPhotobooth
//
//  Created by Sastra Winarta on 13/10/12.
//
//

#import <Foundation/Foundation.h>

@interface Photo : NSObject{
    NSString* _imageData;
}

@property (nonatomic, strong) NSString* imageData;
@property (nonatomic, strong) NSString* photoId;
@property (nonatomic) BOOL isArchive;
@property (nonatomic, strong) NSNumber* numberOfCopy;

@end
