//
//  TemplateViewController.h
//  IPhotobooth
//
//  Created by Sastra Winarta on 2/10/12.
//
//

#import <UIKit/UIKit.h>
#import "TemplateImageView.h"

@interface TemplateViewController : UIViewController<RKObjectLoaderDelegate>

@property (nonatomic, strong) NSString *sessionId;
@property (nonatomic, strong) NSArray *templateImages;
@property (nonatomic, strong) NSArray *templateRects;
@property (nonatomic, strong) NSArray *sessionPhotos;
@property (nonatomic) BOOL isArchive;

@property (strong, nonatomic) IBOutlet UIView *templateView;
@property (weak, nonatomic) IBOutlet TemplateImageView *imageView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *printButton;

@end
