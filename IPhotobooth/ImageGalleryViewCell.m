//
//  ImageGalleryViewCell.m
//  IPhotobooth
//
//  Created by sastra winarta on 17/5/12.
//  Copyright (c) 2012 sastraw@gtwholdings.com. All rights reserved.
//

#import "ImageGalleryViewCell.h"

@implementation ImageGalleryViewCell

@synthesize object = _object;

- (id) initWithFrame: (CGRect) frame reuseIdentifier: (NSString *) aReuseIdentifier
{
    
    self = [super initWithFrame: frame reuseIdentifier: aReuseIdentifier];
    
    if ( self == nil )
        return ( nil );
        
    UIView* mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 226)];
    [mainView setBackgroundColor:[UIColor clearColor]];

    UIImageView *frameImageView = [[UIImageView alloc] initWithFrame:CGRectMake(18, 8, 284, 204)];
    [frameImageView setBackgroundColor: [UIColor blueColor]];

    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(26, 16, 270, 168)];    

    [mainView addSubview:frameImageView];
    [mainView addSubview:_imageView];

    [self.contentView addSubview:mainView];    
    
    return ( self );

}


- (CALayer *) glowSelectionLayer
{
    return ( _imageView.layer );
}

- (UIImage *) image
{
    return ( _imageView.image );
}

- (void) setImage: (UIImage *) anImage
{
    _imageView.image = anImage;
    [self setNeedsLayout];
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    CGSize imageSize = _imageView.image.size;
    CGRect frame = _imageView.frame;
    CGRect bounds = self.contentView.bounds;
    
    if ( (imageSize.width <= bounds.size.width) &&
        (imageSize.height <= bounds.size.height) )
    {
        return;
    }
    
    // scale it down to fit
    CGFloat hRatio = bounds.size.width / imageSize.width;
    CGFloat vRatio = bounds.size.height / imageSize.height;
    CGFloat ratio = MAX(hRatio, vRatio);
    
    frame.size.width = floorf(imageSize.width * ratio);
    frame.size.height = floorf(imageSize.height * ratio);
    frame.origin.x = floorf((bounds.size.width - frame.size.width) * 0.5);
    frame.origin.y = floorf((bounds.size.height - frame.size.height) * 0.5);
    _imageView.frame = frame;
}

@end
