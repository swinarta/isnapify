//
//  ArchiveViewController.h
//  IPhotobooth
//
//  Created by Sastra Winarta on 11/10/12.
//
//

#import <RestKit/RestKit.h>
#import <UIKit/UIKit.h>
#import "AQGridView.h"

@interface ArchiveViewController : UIViewController <AQGridViewDelegate, AQGridViewDataSource,
RKObjectLoaderDelegate>
{
    NSArray *_imageNames;
}
@property (weak, nonatomic) IBOutlet AQGridView *archiveView;
@end
