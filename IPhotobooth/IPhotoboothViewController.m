//
//  IPhotoboothViewController.m
//  IPhotobooth
//
//  Created by Sastra Winarta on 5/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <RestKit/RestKit.h>
#import "IPhotoboothViewController.h"
#import "AQGridViewCell.h"
#import "ImageGalleryViewCell.h"
#import "Session.h"
#import "ApplicationSettings.h"
#import "TemplateViewController.h"
#import "SVProgressHUD.h"

@implementation IPhotoboothViewController

@synthesize galleryView = _galleryView;

#pragma mark RKObjectLoaderDelegate methods

- (void)loadSessions
{
    [SVProgressHUD showWithStatus:@"Loading Sessions" maskType:SVProgressHUDMaskTypeGradient];
    [[RKObjectManager sharedManager] loadObjectsAtResourcePath:@"/sessions" delegate:self];
}

- (void)objectLoader:(RKObjectLoader *)objectLoader didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    [SVProgressHUD dismiss];
}

- (void)objectLoader:(RKObjectLoader*)objectLoader didLoadObjects:(NSArray*)objects
{
    _imageNames = objects;
    [self.galleryView reloadData];
    [SVProgressHUD dismiss];
}

- (NSUInteger) numberOfItemsInGridView: (AQGridView *) aGridView
{
    return ( _imageNames.count );
}

- (AQGridViewCell *) gridView: (AQGridView *) aGridView
           cellForItemAtIndex: (NSUInteger) index
{

    Session *session = (Session *)_imageNames[index];

    static NSString * SessionCellIdentifier = @"SessionCellIdentifier";
        
    ImageGalleryViewCell * cell = (ImageGalleryViewCell *)[aGridView dequeueReusableCellWithIdentifier:SessionCellIdentifier];
    if ( cell == nil )
    {
        cell = [[ImageGalleryViewCell alloc] initWithFrame: CGRectMake(0.0, 0.0, 320.0, 226.0)                                                 reuseIdentifier: SessionCellIdentifier];        
    }
    
    ApplicationSettings *settings = [ApplicationSettings sharedInstance];
        
    NSString *imageURL = [[settings.serverURL stringByAppendingString:@"/"] stringByAppendingString:session.preview];
    
    cell.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]]];
    cell.object = session;
            
    return cell;
}

- (void) gridView:(AQGridView *)gridView didSelectItemAtIndex:(NSUInteger)index
{
    ImageGalleryViewCell *cell = (ImageGalleryViewCell *)[gridView cellForItemAtIndex:index];
    [self performSegueWithIdentifier:@"ShowTemplate" sender: cell];
    [gridView deselectItemAtIndex:index animated:NO];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    ApplicationSettings *settings = [ApplicationSettings sharedInstance];

    ImageGalleryViewCell *cell = (ImageGalleryViewCell *)sender;
    TemplateViewController *templateController = (TemplateViewController *)segue.destinationViewController;
    templateController.sessionId = ((Session *)cell.object).name;
    templateController.templateImages = settings.templateImages;
    templateController.templateRects = settings.templateRects;
    templateController.isArchive = NO;
    templateController.hidesBottomBarWhenPushed = YES;
    self.navigationController.toolbarHidden = NO;
}

- (CGSize) portraitGridCellSizeForGridView: (AQGridView *) aGridView
{
    return ( CGSizeMake(320.0, 246.0) );
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.galleryView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	self.galleryView.autoresizesSubviews = YES;
	self.galleryView.delegate = self;
	self.galleryView.dataSource = self;
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.galleryView = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.toolbarHidden = YES;
    
    [self loadSessions];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (IBAction)refreshPressed:(id)sender
{
    [self loadSessions];
}


@end
